# adfgvx

> Démonstration "didactique" de la méthode de chiffrement ADFGVX


## Dev

```
$ npm install
```

### Run

```
$ npm start
```

### Build

```
$ npm run build
```

Builds the app for macOS, Linux, and Windows, using [electron-packager](https://github.com/electron-userland/electron-packager).


## License

MIT © [](http://jo-charlet.ch)
