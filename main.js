
$('html').bind('keypress', function(e)
{
   if(e.keyCode == 13)
   {
      return false;
   }
});

function scrollTo(partie){
  $('html, body').animate({
    scrollTop: $(partie).offset().top
  }, 800, function(){

    // Add hash (#) to URL when done scrolling (default click behavior)
    window.location.hash = partie;
  });

}

jQuery.moveColumn = function (table, from, to) {
    var rows = jQuery('tr', table);
    var cols;
    rows.each(function() {
        cols = jQuery(this).children('th, td');
        cols.eq(from).detach().insertBefore(cols.eq(to));
    });
}


function goToChiffrer(){
  document.getElementById('chiffrer').style.display="block";
  document.getElementById('dechiffrer').style.display="none";
}

function goToDechiffrer(){
    document.getElementById('chiffrer').style.display="none";
    document.getElementById('dechiffrer').style.display="block";
}

function fnGenNewTable(){
  text =  document.getElementById('textNewTable').value.toLowerCase();
  text = text.replace(/ /g,"");

  for (i = 0; i < text.length;i++) {
      if((text.match(new RegExp(text[i], "g")) || []).length==2){
        outputHTML = "<a style='color:#F00; font-weight:bold;'>Nouveau contenu incorrect, il y a une répétition de caractère</a>";
        document.getElementById("outputErrorNewText").innerHTML = outputHTML;
        return;
   }
  }

  if (text.length!=36){
    outputHTML = "<a style='color:#F00; font-weight:bold;'>Nouveau contenu incorrect, il n'y a pas tout l'alphabet ni tous les chiffres de 0 à 9</a>";
    document.getElementById("outputErrorNewText").innerHTML = outputHTML;
    return;
  }

  outputHTML = "";
  document.getElementById("outputErrorNewText").innerHTML = outputHTML;
  var td = document.getElementById("tableCryptADFGVX").getElementsByTagName("td")

  for(i=0; i<td.length;i++){
    td[i].innerHTML=text[i];
  }

}


function fnSubstitution() {
  text =  document.getElementById('text').value.toLowerCase();
  var tr = document.getElementById("tableCryptADFGVX").getElementsByTagName("tr");
  var outputTxt = "";
  comparTxt="";
  var i;
	var j;
  var k;
  for (var k = 0; k < text.length; k++) {
      for (i = 0; i < tr.length;i++) {
          td = document.getElementById("tableCryptADFGVX").getElementsByTagName("tr")[i].getElementsByTagName("td");
          for (j = 0; j < td.length; j++) {
                comparTxt= td[j].innerHTML;
                if(text[k]==td[j].innerHTML){
                th=document.getElementsByTagName("th");
                outputTxt= outputTxt + th[j+1].innerHTML;
                outputTxt= outputTxt + th[i+6].innerHTML+ " ";
                }
              }
        }
  }
  document.getElementById("messageText").style.display = "block";
  document.getElementById("output_text").innerHTML = text;
  document.getElementById("output_substitution").innerHTML = outputTxt;
  document.getElementById("partie2").style.display = "block";

  scrollTo("#partie2");
}


function fnTransposition() {
  text =  document.getElementById('cle').value.toUpperCase();
  code = document.getElementById('output_substitution').innerHTML;
  code = code.replace(/ /g,"");
  var outputHTML = "";
  var i;

  for (i = 0; i < text.length;i++) {
      if((text.match(new RegExp(text[i], "g")) || []).length==2){
        outputHTML = "<a style='color:#F00; font-weight:bold;'>Mot clé incorrect, il y a une répétition de caractère</a>";
        document.getElementById("output_transposition").innerHTML = outputHTML;
        return;
   }
  }

  outputHTML = "<table id='tableTransposition' class='table table-bordered'><tr>";

  for (i = 0; i < text.length;i++) {
    outputHTML = outputHTML + "<th>" + text[i] + "</th>";
  }

  for (j = 0; j < code.length;j++) {
    if (j% text.length == 0  ){
      outputHTML = outputHTML + "</tr><tr>";
   }

    outputHTML = outputHTML + "<td>" + code[j] + "</td>";
  }

  outputHTML=outputHTML + "</tr></table>"
  document.getElementById("messageGrille").style.display="block";
  document.getElementById("output_transposition").innerHTML = outputHTML;

  num_lastTR=document.getElementById("tableTransposition").getElementsByTagName("tr").length;
  nbTD= document.getElementById("tableTransposition").getElementsByTagName("tr")[num_lastTR-1].getElementsByTagName("td").length;

  // caractères random pour les cellules menquante
  for (k = 0; k < (text.length-nbTD);k++){
    var newTD = document.createElement('td');
    var possible = "ADFGVX";
    newTD.innerHTML= possible.charAt(Math.floor(Math.random() * possible.length));
    document.getElementById("tableTransposition").getElementsByTagName("tr")[num_lastTR-1].appendChild(newTD);
  }

  document.getElementById("partie3").style.display="block";
  scrollTo("#partie3");

}
function getFirstRow(){
  var val="";
  firtRow=document.getElementById("transpositionDecalage").getElementsByTagName("tr")[0].getElementsByTagName("th");
  for (ii=0; ii<firtRow.length; ii++){
    val= val + firtRow[ii].innerHTML;
  }
  return val;
}

function fnTranspositionDecalage(){
  document.getElementById("transpositionDecalage").innerHTML="";

  tableTransposition=document.getElementById("tableTransposition");
  var newTable = tableTransposition.cloneNode(true);
  newTable.id="tableTranspositionDecalage";
  document.getElementById("transpositionDecalage").appendChild(newTable);

  var alphabet = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
  var tbl = $('#tableTranspositionDecalage');
  lenghtText=getFirstRow().length;
  for (a=0; a<alphabet.length; a++){

    for (i=0; i<lenghtText;i++){
      text=getFirstRow();
      if(alphabet[a]==text[i]){
            jQuery.moveColumn(tbl, i, 0);
    }
  }
}

  document.getElementById("partie4").style.display="block";
  scrollTo("#partie4");
}

function fnGetCrypter(){
  code_ADFGVX="";
  var th =document.getElementById("tableTranspositionDecalage").getElementsByTagName("th");
  for (i=0; i<th.length; i++){
      tr=document.getElementById("tableTranspositionDecalage").getElementsByTagName("tr");
      for (j=0; j<tr.length; j++){
          //exclure la ligne de titre
          if (j!=0){
          td=tr[j].getElementsByTagName("td")[i];
          code_ADFGVX= code_ADFGVX + td.innerHTML;
          }
      }
  }

document.getElementById("output_ADFGVX").innerHTML = code_ADFGVX;

scrollTo("#output_ADFGVX");
}
//------------------ decrypt -----------------------------------------------
function getFirstRowDecrypt(){
  var val="";
  firtRow=document.getElementById("DecryptTableau2").getElementsByTagName("tr")[0].getElementsByTagName("th");
  for (ii=0; ii<firtRow.length; ii++){
    val= val + firtRow[ii].innerHTML;
  }
  return val;
}
function getFirstRowDecrypt2(){
  var val="";
  firtRow=document.getElementById("DecryptTableau4").getElementsByTagName("tr")[0].getElementsByTagName("th");
  for (ii=0; ii<firtRow.length; ii++){
    val= val + firtRow[ii].innerHTML;
  }
  return val;
}



function fnDecrypt1(){
  var outputHTML = "";
  //code = document.getElementById('decrypText').innerHTML;
  //code = code.replace(/ /g,"");
  text =  document.getElementById('decryptCle').value.toUpperCase();
  outputHTML = "<table id='DecryptTableau1' class='table table-bordered'><tr>";

  for (i = 0; i < text.length;i++) {
    outputHTML = outputHTML + "<th>" + text[i] + "</th>";
  }
  outputHTML=outputHTML + "</tr></table>"

  document.getElementById("outputDecryptTableau1").innerHTML = outputHTML;

  document.getElementById("decryptPartie2").style.display="block";
  scrollTo("#decryptPartie2");

}


function fnDecrypt2(){
  document.getElementById("outputDecryptTableau2").innerHTML="";
  tableDecrypt2=document.getElementById("DecryptTableau1");
  var newTableDecrypt = tableDecrypt2.cloneNode(true);
  newTableDecrypt.id="DecryptTableau2";
  document.getElementById("outputDecryptTableau2").appendChild(newTableDecrypt);

  var alphabet = "ZYXWVUTSRQPONMLKJIHGFEDCBA";
  var tbl = $('#DecryptTableau2');
  lenghtText=getFirstRowDecrypt().length;
  for (a=0; a<alphabet.length; a++){

    for (i=0; i<lenghtText;i++){
      text=getFirstRowDecrypt();
      if(alphabet[a]==text[i]){
            jQuery.moveColumn(tbl, i, 0);
          }
    }
  }
  document.getElementById("decryptPartie3").style.display="block";
  scrollTo("#decryptPartie3");
}


function fnDecrypt3(){
  document.getElementById("outputDecryptTableau3").innerHTML="";

  cle =  document.getElementById('decryptCle').value.toUpperCase();
  text =  document.getElementById('decrypText').value.toUpperCase();
  text = text.replace(/ /g,"");
  tableDecrypt3=document.getElementById("DecryptTableau2");
  var newTableDecrypt = tableDecrypt3.cloneNode(true);
  newTableDecrypt.id="DecryptTableau3";

  document.getElementById("outputDecryptTableau3").appendChild(newTableDecrypt);

  tableDecrypt3=document.getElementById("outputDecryptTableau3").innerHTML;
  tableDecrypt3=tableDecrypt3.slice(0, (tableDecrypt3.length-16));

  for (j = 0; j < text.length;j++) {
    if (j% cle.length == 0  ){
      tableDecrypt3 = tableDecrypt3 + "</tr><tr>";
   }
    tableDecrypt3 = tableDecrypt3 + "<td>" + text[j] + "</td>";
  }

    tableDecrypt3 = tableDecrypt3 + "</tr></table>";

    document.getElementById("outputDecryptTableau3").innerHTML=tableDecrypt3;

    var curLetter=0;
    var th =document.getElementById("outputDecryptTableau3").getElementsByTagName("th");
    for (i=0; i<th.length; i++){
        tr=document.getElementById("outputDecryptTableau3").getElementsByTagName("tr");
        for (j=0; j<tr.length; j++){
            //exclure la ligne de titre
            if (j!=0){
            td=tr[j].getElementsByTagName("td")[i].innerHTML=text[curLetter];
            curLetter=curLetter+1;
            }
        }
    }
    document.getElementById("decryptPartie4").style.display="block";
    scrollTo("#decryptPartie4");
}

function fnDecrypt4(){
    document.getElementById("outputDecryptTableau4").innerHTML="";
  tableDecrypt4=document.getElementById("DecryptTableau3");
  var newTableDecrypt = tableDecrypt4.cloneNode(true);
  newTableDecrypt.id="DecryptTableau4";
  document.getElementById("outputDecryptTableau4").appendChild(newTableDecrypt);

  var cleRevers = document.getElementById('decryptCle').value.toUpperCase();
  cleRevers= cleRevers.split('').reverse().join('');

  var tbl = $('#DecryptTableau4');
  lenghtText=getFirstRowDecrypt2().length;
  for (a=0; a<cleRevers.length; a++){
    for (i=0; i<lenghtText;i++){
      text=getFirstRowDecrypt2();
      if(cleRevers[a]==text[i]){
            jQuery.moveColumn(tbl, i, 0);
          }
    }
  }
  document.getElementById("decryptPartie5").style.display="block";
  scrollTo("#decryptPartie5");
}

function fnDecrypt5(){
  text="";
  td=document.getElementById("outputDecryptTableau4").getElementsByTagName("td");

  for (i=0; i<td.length; i++){
    if (i % 2 == 0) {
      text= text + " ";
    }
    text= text + td[i].innerHTML;
  }

  document.getElementById("outputDecryptTableau5").innerHTML=text;
  document.getElementById("decryptPartie6").style.display="block";
  scrollTo("#decryptPartie6");
}

function fnDecrypt6(){
    coordonnee =   document.getElementById("outputDecryptTableau5").innerHTML;
    coordonnee = coordonnee.replace(/ /g,"");
    th1=document.getElementById("tableDecryptADFGVX").getElementsByTagName("tr")[0].getElementsByTagName("th");
    th=document.getElementById("tableDecryptADFGVX").getElementsByTagName("th");
    var numTD;
    var numTR;
    textDecrypte = ""
    for (i=0; i<(coordonnee.length); i++){
        for (j=0; j<th1.length;j++){
          x=th1[j].innerHTML;
          x = x.replace(/ /g,"");
          if(x==coordonnee[i]){
            numTD=j-1;
          }
        }
        i=i+1;
        for (k=0; k<th.length-6;k++){
          y=th[k+6].innerHTML;
          y = y.replace(/ /g,"");
          if(y==coordonnee[i]){
            numTR=k;
          }
        }
      //  numTD=parseInt(numTD);
      //  numTR=parseInt(numTR);
        lettre = document.getElementById("tableDecryptADFGVX").getElementsByTagName("tr")[numTR].getElementsByTagName("td")[numTD].innerHTML;
        lettre = lettre.replace(/ /g,"");
        console.log(lettre);
        textDecrypte = textDecrypte+lettre;
    }
  //  console.log(textDecrypte);
    document.getElementById("outputDecryptTableau6").innerHTML=textDecrypte;
}

function fnGenNewTableDecrypt(){
  text =  document.getElementById('textNewTableDecrypt').value.toLowerCase();
  text = text.replace(/ /g,"");

  for (i = 0; i < text.length;i++) {
      if((text.match(new RegExp(text[i], "g")) || []).length==2){
        outputHTML = "<a style='color:#F00; font-weight:bold;'>Nouveau contenu incorrect, il y a une répétition de caractère</a>";
        document.getElementById("outputErrorNewTextDecrypt").innerHTML = outputHTML;
        return;
   }
  }

  if (text.length!=36){
    outputHTML = "<a style='color:#F00; font-weight:bold;'>Nouveau contenu incorrect, il n'y a pas tout l'alphabet ni tous les chiffres de 0 à 9</a>";
    document.getElementById("outputErrorNewTextDecrypt").innerHTML = outputHTML;
    return;
  }

  outputHTML = "";
  document.getElementById("outputErrorNewTextDecrypt").innerHTML = outputHTML;
  var td = document.getElementById("tableDecryptADFGVX").getElementsByTagName("td")

  for(i=0; i<td.length;i++){
    td[i].innerHTML=text[i];
  }
}
